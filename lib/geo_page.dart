import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:poc/main.dart';

class GeoPage extends StatefulWidget {
  const GeoPage({Key? key}) : super(key: key);

  @override
  _GeoPageState createState() => _GeoPageState();
}

class _GeoPageState extends State<GeoPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Geolocation'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: _geoPressed,
          child: const Text('GET LOCATION'),
        ),
      ),
    );
  }

  void _geoPressed() async {
    bool serviceEnabled;
    LocationPermission permission;

    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      return;
    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
    }
    if (permission != LocationPermission.denied) {
      Toast.show(context, 'Resolving location...');
      var position = await Geolocator.getCurrentPosition();
      Toast.show(context, position.toString());
    }
  }
}
