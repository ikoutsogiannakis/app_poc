import 'package:flutter/material.dart';
import 'dart:async';
import 'package:camera/camera.dart';
import 'package:poc/main.dart';

class CameraPage extends StatefulWidget {
  const CameraPage({Key? key}) : super(key: key);

  @override
  _CameraPageState createState() => _CameraPageState();
}

class _CameraPageState extends State<CameraPage> {
  CameraController? controller;
  bool flashIsOn = false;
  @override
  void initState() {
    super.initState();
    CameraDescription cam = cameras![0];
    controller = CameraController(cam, ResolutionPreset.max);
    controller!.initialize().then((_) {
      if (!mounted) {
        return;
      }
      controller!.setFlashMode(FlashMode.off);
      setState(() {});
    });
  }

  @override
  void dispose() {
    controller?.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!controller!.value.isInitialized) {
      return Container();
    }
    return Scaffold(
      body: Stack(
        children: [
          CameraPreview(controller!),
          Container(
            alignment: Alignment.bottomCenter,
            margin: const EdgeInsets.only(bottom: 30),
            child: FloatingActionButton(
              onPressed: _buttonPressed,
              child: const Icon(Icons.camera),
            ),
          ),
          Container(
            width: 50,
            alignment: Alignment.topLeft,
            margin: const EdgeInsets.only(top: 20),
            child: MaterialButton(
              onPressed: _backPressed,
              child: const Icon(
                Icons.arrow_back,
                color: Colors.white,
              ),
            ),
          ),
          Container(
            alignment: Alignment.topRight,
            margin: const EdgeInsets.only(top: 20),
            child: Switch(
              value: flashIsOn,
              onChanged: flashChanged,
            ),
          ),
        ],
      ),
    );
  }

  void _backPressed() {
    Navigator.of(context).pop();
  }

  void _buttonPressed() {
    takePicture().then((XFile? file) {
      if (mounted) {
        Navigator.of(context)
            .pushNamed(NavRoute.camprev.id, arguments: file!.path);
      }
    });
  }

  Future<XFile?> takePicture() async {
    final CameraController? cameraController = controller;
    if (cameraController == null || !cameraController.value.isInitialized) {
      Toast.show(context, 'Error: select a camera first.');
      return null;
    }

    if (cameraController.value.isTakingPicture) {
      // A capture is already pending, do nothing.
      return null;
    }

    try {
      XFile file = await cameraController.takePicture();
      return file;
    } on CameraException catch (e) {
      Toast.show(context, e.description!);
      return null;
    }
  }

  void flashChanged(bool value) async {
    setState(() {
      flashIsOn = !flashIsOn;
    });
    await controller!
        .setFlashMode(flashIsOn ? FlashMode.always : FlashMode.off);
  }
}
