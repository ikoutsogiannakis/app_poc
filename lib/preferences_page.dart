import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PreferencesPage extends StatefulWidget {
  const PreferencesPage({Key? key}) : super(key: key);

  @override
  _PreferencesPageState createState() => _PreferencesPageState();
}

class _PreferencesPageState extends State<PreferencesPage> {
  SharedPreferences? prefs;
  bool isOn = false;
  bool isChecked = false;
  String? text;
  TextEditingController? controller;
  @override
  void initState() {
    super.initState();
    SharedPreferences.getInstance().then((value) {
      prefs = value;
      setState(() {
        isOn = prefs!.getBool('isOn') ?? false;
        isChecked = prefs!.getBool('isChecked') ?? false;
        controller = TextEditingController(text: prefs!.getString('text'));
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Preferences'),
      ),
      body: Padding(
        padding: const EdgeInsets.all(30),
        child: Wrap(
          alignment: WrapAlignment.center,
          runSpacing: 30,
          children: [
            Switch(
              onChanged: _switchChanged,
              value: isOn,
            ),
            TextField(
              controller: controller,
              onChanged: _textChanged,
            ),
            Checkbox(
              value: isChecked,
              onChanged: _checkChanged,
            ),
          ],
        ),
      ),
    );
  }

  void _switchChanged(bool value) {
    setState(() {
      isOn = value;
    });
    prefs!.setBool('isOn', value);
  }

  void _checkChanged(bool? value) {
    setState(() {
      isChecked = value ?? false;
    });
    prefs!.setBool('isChecked', value!);
  }

  void _textChanged(String value) {
    prefs!.setString('text', value);
  }
}
