import 'package:flutter/material.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

import 'main.dart';

class ConnectivityPage extends StatefulWidget {
  const ConnectivityPage({Key? key}) : super(key: key);

  @override
  _ConnectivityPageState createState() => _ConnectivityPageState();
}

class _ConnectivityPageState extends State<ConnectivityPage> {
  @override
  initState() {
    super.initState();
    Connectivity().onConnectivityChanged.listen((ConnectivityResult result) {
      Toast.show(context, result.toString());
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Connectivity'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: _buttonPressed,
          child: const Text('CHECK'),
        ),
      ),
    );
  }

  void _buttonPressed() async {
    var conn = await (Connectivity().checkConnectivity());
    Toast.show(context, conn.toString());
  }
}
