import 'dart:io';
import 'package:flutter/material.dart';

class CameraPreviewPage extends StatefulWidget {
  const CameraPreviewPage({Key? key}) : super(key: key);

  @override
  _CameraPreviewPageState createState() => _CameraPreviewPageState();
}

class _CameraPreviewPageState extends State<CameraPreviewPage> {
  String? imagePath;
  @override
  void initState() {
    super.initState();
    Future.delayed(Duration.zero, () {
      setState(() {
        imagePath = ModalRoute.of(context)!.settings.arguments.toString();
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: [
        imagePath != null ? Image.file(File(imagePath!)) : Container(),
        Container(
          width: 50,
          alignment: Alignment.topLeft,
          margin: const EdgeInsets.only(top: 20),
          child: MaterialButton(
            onPressed: _backPressed,
            child: const Icon(
              Icons.arrow_back,
              color: Colors.white,
            ),
          ),
        ),
      ],
    );
  }

  void _backPressed() {
    Navigator.of(context).pop();
  }
}
