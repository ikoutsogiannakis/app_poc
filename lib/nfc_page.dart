import 'package:flutter/material.dart';
import 'package:nfc_manager/nfc_manager.dart';
import 'package:poc/main.dart';

class NfcPage extends StatefulWidget {
  const NfcPage({Key? key}) : super(key: key);

  @override
  _NfcPageState createState() => _NfcPageState();
}

class _NfcPageState extends State<NfcPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('NFC'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: _buttonPressed,
          child: const Text('SCAN'),
        ),
      ),
    );
  }

  void _buttonPressed() async {
    if (await NfcManager.instance.isAvailable()) {
      NfcManager.instance.startSession(
        onDiscovered: (NfcTag tag) async {
          Ndef? ndef = Ndef.from(tag);
          NdefMessage? message = ndef!.cachedMessage;
          var data = message!.records.first.payload.skip(3);
          Toast.show(context, String.fromCharCodes(data));
          NfcManager.instance.stopSession();
        },
      );
    }
  }
}
