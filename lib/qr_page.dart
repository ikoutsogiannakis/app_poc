import 'package:flutter/material.dart';
import 'package:poc/main.dart';
import 'package:qr_code_scanner/qr_code_scanner.dart';

class QrPage extends StatefulWidget {
  const QrPage({Key? key}) : super(key: key);

  @override
  _QrPageState createState() => _QrPageState();
}

class _QrPageState extends State<QrPage> {
  bool isScanning = false;
  final GlobalKey qrKey = GlobalKey(debugLabel: 'QR');
  QRViewController? controller;
  Barcode? result;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('QR Code'),
      ),
      body: Center(
        child: isScanning
            ? _getQRView()
            : ElevatedButton(
                onPressed: _buttonPressed,
                child: const Text('SCAN'),
              ),
      ),
    );
  }

  Widget _getQRView() {
    return QRView(
      key: qrKey,
      onQRViewCreated: _onQRViewCreated,
      overlay: QrScannerOverlayShape(
        cutOutSize: 160,
        borderWidth: 10,
        borderColor: Colors.green,
        borderRadius: 10,
      ),
    );
  }

  void _buttonPressed() {
    setState(() {
      isScanning = true;
    });
  }

  void _onQRViewCreated(QRViewController controller) {
    this.controller = controller;
    controller.scannedDataStream.listen((scanData) {
      controller.dispose();
      setState(() {
        result = scanData;
        isScanning = false;
      });
      Toast.show(context, result!.code);
    });
  }
}
