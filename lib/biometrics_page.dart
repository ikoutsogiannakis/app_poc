import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';

import 'main.dart';

class BiometricsPage extends StatefulWidget {
  const BiometricsPage({Key? key}) : super(key: key);

  @override
  _BiometricsPageState createState() => _BiometricsPageState();
}

class _BiometricsPageState extends State<BiometricsPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Biometrics'),
      ),
      body: Center(
        child: ElevatedButton(
          child: const Text('AUTH'),
          onPressed: _buttonPressed,
        ),
      ),
    );
  }

  void _buttonPressed() async {
    var localAuth = LocalAuthentication();
    if (await localAuth.canCheckBiometrics) {
      if (await localAuth.authenticate(
          localizedReason: 'Please authenticate')) {
        Toast.show(context, 'Auth successful');
      }
    }
  }
}
