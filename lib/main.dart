import 'package:flutter/material.dart';
import 'package:poc/biometrics_page.dart';
import 'package:poc/camera_page.dart';
import 'package:poc/camera_preview_page.dart';
import 'package:poc/connectivity_page.dart';
import 'package:poc/geo_page.dart';
import 'package:poc/map_page.dart';
import 'package:poc/nfc_page.dart';
import 'package:poc/preferences_page.dart';
import 'package:poc/qr_page.dart';
import './home_page.dart';
import 'package:camera/camera.dart';

List<CameraDescription>? cameras;

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  cameras = await availableCameras();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(primarySwatch: Colors.green),
      home: const HomePage(),
      routes: <String, WidgetBuilder>{
        NavRoute.conn.id: (BuildContext ctx) => const ConnectivityPage(),
        NavRoute.nfc.id: (BuildContext ctx) => const NfcPage(),
        NavRoute.geo.id: (BuildContext ctx) => const GeoPage(),
        NavRoute.qr.id: (BuildContext ctx) => const QrPage(),
        NavRoute.map.id: (BuildContext ctx) => const MapPage(),
        NavRoute.prefs.id: (BuildContext ctx) => const PreferencesPage(),
        NavRoute.bio.id: (BuildContext ctx) => const BiometricsPage(),
        NavRoute.cam.id: (BuildContext ctx) => const CameraPage(),
        NavRoute.camprev.id: (BuildContext ctx) => const CameraPreviewPage(),
      },
    );
  }
}

class Toast {
  static void show(BuildContext context, String message) {
    ScaffoldMessenger.of(context).showSnackBar(
      SnackBar(
        content: Text(message),
        duration: const Duration(seconds: 5),
      ),
    );
  }
}

class NavRoute {
  static final NavRoute conn = NavRoute('Connectivity', '/conn');
  static final NavRoute nfc = NavRoute('NFC', '/nfc');
  static final NavRoute geo = NavRoute('Geolocation', '/geo');
  static final NavRoute qr = NavRoute('QR Code', '/qr');
  static final NavRoute map = NavRoute('Map', '/map');
  static final NavRoute prefs = NavRoute('Preferences', '/prefs');
  static final NavRoute bio = NavRoute('Biometrics', '/bio');
  static final NavRoute cam = NavRoute('Camera', '/cam');
  static final NavRoute camprev = NavRoute('Camera Preview', '/camprev');
  static final List<NavRoute> homeRoutes = [
    conn,
    nfc,
    geo,
    qr,
    map,
    prefs,
    bio,
    cam,
  ];
  String name;
  String id;
  NavRoute(this.name, this.id);
}
