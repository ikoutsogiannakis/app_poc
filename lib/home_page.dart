import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

import 'main.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    final myBoxDecoration = BoxDecoration(
        color: Colors.white,
        border: Border.all(color: Colors.grey),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.5),
            spreadRadius: 1,
            blurRadius: 3,
            offset: const Offset(3, 3), // changes position of shadow
          ),
        ],
        borderRadius: const BorderRadius.all(Radius.circular(8.0)));

    return Scaffold(
      appBar: AppBar(
        title: const Text('Flutter POC'),
      ),
      body: SingleChildScrollView(
        child: Center(
          child: Container(
            padding: const EdgeInsets.all(16),
            child: Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  Container(
                    padding: const EdgeInsets.only(top: 36),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Image.asset('assets/images/logo_gray.png', width: 200)
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 8, bottom: 24),
                    child: const Text('What would you like to do?'),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Container(
                        height: 100,
                        padding: const EdgeInsets.all(12.0),
                        margin: const EdgeInsets.only(bottom: 20),
                        decoration: myBoxDecoration,
                        child: const Text('Column 1 Layout test'),
                      ),
                      Container(
                        height: 100,
                        padding: const EdgeInsets.all(12.0),
                        margin: const EdgeInsets.only(bottom: 20),
                        decoration: myBoxDecoration,
                        child: const Text('Column 2 Layout test'),
                      )
                    ],
                  ),
                  for (var i = 0; i < NavRoute.homeRoutes.length; i++)
                    SectionButton(i, NavRoute.homeRoutes[i].name, _itemSelected)
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  void _itemSelected(int index) async {
    Navigator.of(context).pushNamed(NavRoute.homeRoutes[index].id);
  }
}

class SectionButton extends StatefulWidget {
  final int _index;
  final String _text;
  final Function(int) _onPressed;
  const SectionButton(this._index, this._text, this._onPressed, {Key? key})
      : super(key: key);

  @override
  _SectionButtonState createState() => _SectionButtonState();
}

class _SectionButtonState extends State<SectionButton> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      child: ElevatedButton(
        onPressed: () => widget._onPressed(widget._index),
        child: Text(widget._text),
      ),
    );
  }
}
